package com.techu.apitechudb.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @RequestMapping("/")
    public String index() {
        return "super.toString()";
    }

    @RequestMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "Tom") String name) {
        return String.format("Hola: %s!", name);
    }
}
//ProductController
//ProductService
//Conexión a BD
//ProductModel
