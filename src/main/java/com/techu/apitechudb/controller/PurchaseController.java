package com.techu.apitechudb.controller;


import com.techu.apitechudb.model.ProductModel;
import com.techu.apitechudb.model.PurchaseModel;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @PostMapping("/purchase")
    public ResponseEntity<PurchaseServiceResponse> addProduct(@RequestBody PurchaseModel purchase){
        System.out.println(" ::addProduct:: " + purchase.getId());

        PurchaseServiceResponse result = this.purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(result, result.getResponseHttpStatusCode());
    }

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases() {
        System.out.println("getPurchases");

        return new ResponseEntity<>(
                this.purchaseService.getPurchases(),
                HttpStatus.OK
        );
    }
}
