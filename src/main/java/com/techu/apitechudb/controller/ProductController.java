package com.techu.apitechudb.controller;

import com.techu.apitechudb.model.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println(" ::getProducts:: ");

        return new ResponseEntity<>(
                this.productService.findAll()
                , HttpStatus.OK
        );
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id) {
        System.out.println(" ::getProductById:: " + id);

        Optional<ProductModel> result = this.productService.getById(id);
        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "No existe el producto",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println(" ::addProduct:: " + product.getId());

        return  new ResponseEntity<>(
                this.productService.add(product)
                , HttpStatus.CREATED
        );
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println(" ::updateProduct:: " + id);

        Optional<ProductModel> result = this.productService.getById(id);

        if (result.isPresent())
            this.productService.update(product);

        return new ResponseEntity<>(
                product
                , result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<ProductModel> deleteProduct(@PathVariable String id){
        System.out.println(" ::deleteProduct:: " + id);
        boolean delete = this.productService.delete(id);

        return  new ResponseEntity(
                delete ? "Producto borrado" : "Producto no borrado"
                , delete ? HttpStatus.OK: HttpStatus.NOT_FOUND
        );
    }
}
