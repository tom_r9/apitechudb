package com.techu.apitechudb.controller;

import com.techu.apitechudb.model.ProductModel;
import com.techu.apitechudb.model.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;


    /*@GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers() {
        System.out.println(" ::getUsers:: ");

        return new ResponseEntity<>(
                this.userService.findAll()
                , HttpStatus.OK
        );
    }*/

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println(" ::getUserById:: " + id);

        Optional<UserModel> result = this.userService.getById(id);
        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "No existe el usuario",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @GetMapping("/users")
    public ResponseEntity<Object> getOrderAll(@RequestParam(name="data", required = false) String data) {
        System.out.println(" ::getOrderAll:: " + data);
        return new ResponseEntity<>(
                this.userService.orderAll(data),
                HttpStatus.OK
        );
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println(" ::addUser:: " + user.getId());

        return  new ResponseEntity<>(
                this.userService.add(user)
                , HttpStatus.CREATED
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println(" ::updateUser:: " + id);

        Optional<UserModel> result = this.userService.getById(id);

        UserModel nu = new UserModel();

        if (result.isPresent()) {
            nu.setId(id);
            nu.setAge(user.getAge());
            nu.setName(user.getName());
            this.userService.update(nu);
        }
        return new ResponseEntity<>(
                user
                , result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<UserModel> deleteUser(@PathVariable String id){
        System.out.println(" ::deleteUser:: " + id);
        boolean delete = this.userService.delete(id);

        return  new ResponseEntity(
                delete ? "Usuario borrado" : "No existe el usuario"
                , delete ? HttpStatus.OK: HttpStatus.NOT_FOUND
        );
    }

}
