package com.techu.apitechudb.services;

import com.techu.apitechudb.model.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        System.out.println(" ::findAll:: ");
        return this.productRepository.findAll();
    }

    public Optional<ProductModel> getById(String id) {
        System.out.println(" ::getById:: ");
        return this.productRepository.findById(id);
    }

    public ProductModel add(ProductModel product) {
        System.out.println(" ::add:: ");
        return this.productRepository.save(product);
    }

    public ProductModel update(ProductModel product) {
        System.out.println(" ::update:: ");
        return this.productRepository.save(product);
    }

    public boolean delete(String id) {
        System.out.println(" ::delete:: ");
        boolean result = false;

        if (this.getById(id).isPresent() == true) {
            this.productRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}
