package com.techu.apitechudb.services;

import com.techu.apitechudb.model.ProductModel;
import com.techu.apitechudb.model.PurchaseModel;
import com.techu.apitechudb.model.UserModel;
import com.techu.apitechudb.repositories.ProductRepository;
import com.techu.apitechudb.repositories.PurchaseRepository;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    UserService userService;

    @Autowired
    ProductService productService;

    @Autowired
    PurchaseRepository purchaseRepository;

    public PurchaseServiceResponse addPurchase(PurchaseModel purchase) {
        System.out.println("addPurchase");

        PurchaseServiceResponse result = new PurchaseServiceResponse();

        result.setPurchase(purchase);

        if (this.userService.getById(purchase.getUserId()).isPresent()) {
            System.out.println("El usuario de la compra no se ha encontrado");

            result.setMsg("El usuario de la compra no se ha encontrado");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        if (this.purchaseRepository.findById(purchase.getId()).isPresent() == true) {
            System.out.println("Ya hay una compra con esa id");

            result.setMsg("Ya hay una compra con esa id");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        float amount = 0;
        for (Map.Entry<String, Integer> purchaseItem : purchase.getItems().entrySet()) {
            if (this.productService.getById(purchaseItem.getKey()).isPresent()) {
                System.out.println("El producto con la id " + purchaseItem.getKey()
                        + " no se encuentra en el sistema");

                result.setMsg("El producto con la id " + purchaseItem.getKey()
                        + " no se encuentra en el sistema");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

                return result;
            } else {
                System.out.println("Añadiendo valor de " + purchaseItem.getValue()
                        + " unidades del producto al total");

                amount +=
                        (this.productService.getById(purchaseItem.getKey()).get().getPrice()
                                * purchaseItem.getValue());
            }
        }

        purchase.setAmount(amount);
        this.purchaseRepository.save(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.CREATED);

        return result;
    }

    public List<PurchaseModel> getPurchases() {
        System.out.println("getPurchases");

        return this.purchaseRepository.findAll();
    }
}
