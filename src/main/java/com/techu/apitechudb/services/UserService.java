package com.techu.apitechudb.services;

import com.techu.apitechudb.model.ProductModel;
import com.techu.apitechudb.model.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll() {
        System.out.println(" ::findAll Users:: ");
        return this.userRepository.findAll();
    }

    public List<UserModel> orderAll(String data) {
        System.out.println(" ::orderAll Users:: ");
        List<UserModel> user = this.userRepository.findAll();
        /*
        * if(order != null)
        *       order = this.userRepository.findAll(Sort.by("age"));
        * else
        *    order = this.userRepository.findAll();
        * */
        if(data != null) {
            if (data.equals("A")) {
                Collections.sort(user, new Comparator<UserModel>() {
                    @Override
                    public int compare(UserModel p1, UserModel p2) {
                        return new Integer(p1.getAge()).compareTo(new Integer(p2.getAge()));
                    }
                });
            } else if (data.equals("D")) {
                Collections.sort(user, new Comparator<UserModel>() {
                    @Override
                    public int compare(UserModel p1, UserModel p2) {
                        return new Integer(p2.getAge()).compareTo(new Integer(p1.getAge()));
                    }
                });
            }
        }
        return user;
    }

    public Optional<UserModel> getById(String id) {
        System.out.println(" ::getById User:: ");
        return this.userRepository.findById(id);
    }

    public UserModel add(UserModel user) {
        System.out.println(" ::add User:: ");
        return this.userRepository.save(user);
    }

    public UserModel update(UserModel user) {
        System.out.println(" ::update:: ");
        return this.userRepository.save(user);
    }

    public boolean delete(String id) {
        System.out.println(" ::delete:: ");
        boolean result = false;

        if (this.getById(id).isPresent() == true) {
            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }

}
