package com.techu.apitechudb.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Map;

@Document(collection = "purchases")
public class PurchaseModel {

    @Id
    private String id;
    private String userId;
    private float amount;
    private Map<String, Integer> items;

    public PurchaseModel(String id, String userId, float amount, Map<String, Integer> items) {
        this.id = id;
        this.userId = userId;
        this.amount = amount;
        this.items = items;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Map<String, Integer> getItems() {
        return items;
    }

    public void setItems(Map<String, Integer> items) {
        this.items = items;
    }

    public PurchaseModel(){

    }

}
