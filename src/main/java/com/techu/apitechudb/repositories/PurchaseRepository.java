package com.techu.apitechudb.repositories;

import com.techu.apitechudb.model.PurchaseModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseRepository extends MongoRepository<PurchaseModel, String> {
}
